module.exports = function makeDeleteMovie(deletemovi) {
    return async function deletemovie(httpRequest) {
        try {
            const id = httpRequest.params.id

            const posted = await deletemovi(id);

            return {
                headers: {
                    'Content-Type': 'application/json',
                },
                statusCode: 201,
                body: "Data Deleted Sucessfully"
            }
        } catch (e) {
            console.log(e)

            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: e.message
                }
            }
        }
    }
}