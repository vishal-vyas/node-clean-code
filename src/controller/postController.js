module.exports = function makePostMovie (postMovies) {
    return async function postMovie(httpRequest) {
      try {
        
        const { ...movieInfo } = httpRequest.body
        console.log('mov',movieInfo)
        const posted = await postMovies(movieInfo.moviename,movieInfo.realeaseyear);
        return {
          headers: {
            'Content-Type': 'application/json',
        
          },
          statusCode: 201,
          body: "Movie Added Sucessfully"
        }
      } catch (e) {
        // TODO: Error logging
        console.log(e)
  
        return {
          headers: {
            'Content-Type': 'application/json'
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        }
      }
    }
  }