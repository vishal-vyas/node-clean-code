const usecase = require('../use-cases');
const makeGetMovies  = require('./getController');
const makePostMovie = require('./postController');
const makePutMovie = require('./putController');

const makeDeleteMovie = require('./delController');
const finalpostMovie = makePostMovie(usecase.submitMovie);
const finalMovie = makeGetMovies(usecase.showMovie);
const finalputMovie = makePutMovie(usecase.putNewMovie);
const finaldelMovie = makeDeleteMovie(usecase.delMovie);
// console.log(showMovie);
module.exports = {finalMovie,finalpostMovie,finalputMovie,finaldelMovie}