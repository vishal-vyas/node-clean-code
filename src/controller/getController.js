module.exports = function makeGetMovies ( getMoviess ) {
    return async function getMovies (httpRequest) {
      const headers = {
        'Content-Type': 'application/json'
      }
      try {
        const getmovie = await getMoviess

            return {
            headers,
            statusCode: 200,
            body: getmovie
          }
      } catch (e) {
        // TODO: Error logging
        console.log(e)
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        }
      }
    }
  }