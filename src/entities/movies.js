module.exports = function buildMakeMovie(){
    return function MakeMovie({
        movieName,
        releaseYear
    } = {}){
        return Object.freeze(
            {
                getMovieName : () => movieName,
                getReleaseYear : () => releaseYear
            });
    };
}
