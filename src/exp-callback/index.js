const config = require('../config/development');
module.exports = function makeExpressCallback(controller) {
    return (req, res) => {
      const httpRequest = {
        body: req.body,
        query: req.query,
        params: req.params,
        ip: req.ip,
        method: req.method,
        path: req.path,
        headers: {
          'Content-Type': req.get('Content-Type'),
          Referer: req.get('referer'),
          'User-Agent': req.get('User-Agent'),
          'Authorization' :req.get('Authorization')
        }
      }
      controller(httpRequest)
        .then((httpResponse) => {
          if ( httpRequest.headers.Authorization == 'authSucess123')
             {
            // res.set(httpResponse.headers)
            res.type('json')
            res.status(httpResponse.statusCode).send(httpResponse.body)
          }
        
        //   if(httpResponse.headers('Authorization') === config.config.token){
        //     res.send(httpResponse.body)
        //   }
        else{
            res.status(401).send('Unauthorized Acess');
        }
      }
      )
        .catch(e => res.status(500).send({ error: 'An unkown error occurred.' }))
    }
  }

