const config = require('../config/development');

const mysql = require('mysql2');

const con = mysql.createConnection({
    host : config.config.mysql.host,
    user : config.config.mysql.username,
    password : config.config.mysql.password,
    database : config.config.mysql.DbName,
});

const makeMoviesDb = require('./movies-db');

const MovieDB = makeMoviesDb({con});
module.exports = MovieDB;