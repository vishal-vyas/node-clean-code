module.exports = function makeMoviesDb ({con}) {
    return Object.freeze({
      findAll,
      insertMovie,
      updateMovie,
      deleteMovie,
    })
    async function findAll(){
       
        con.connect((err)=>{
            if(err) return console.error('err');
            console.log('sucess...')
        })
       
        const query = `SELECT * from movies`;
        console.log(query);
        const [data] = await con.promise().query({
            sql : query
        });
        if(data){
            // console.log(data);
            return data[0]
        } else{
            console.log('empty')
            return null;
        }
    }
    async function insertMovie(moviename,realeaseyear){
        con.connect((err)=>{
            if(err) return console.error('err');
            console.log('sucess...')
        })
        const mv = {moviename,realeaseyear};
        const insertquery = `INSERT into movies SET ?`;
        console.log(insertquery);
        const [data] = await con.promise().query(`INSERT into movies SET ?`,mv);
        if(data){
            console.log('done',data);
            return data[0];
        } else{
            console.log('fail')
            return null;
        }

    }
    async function updateMovie(moviename,realeaseyear,id){
        con.connect((err)=>{
            if(err) return console.error('err');
            console.log('sucess...')
        })
        const mv = [moviename,realeaseyear,id];
        const [data] = await con.promise().query(`UPDATE movies SET moviename = ?, realeaseyear = ? Where id = ?`,mv);
        if(data){
            console.log('done',data);
            return data[0];
        } else{
            console.log('fail')
            return null;
        }
    }
    async function deleteMovie(id){
        con.connect((err)=>{
            if(err) return console.error('err');
            console.log('sucess...')
        })
        const [data] = await con.promise().query(`DELETE FROM movies Where id = ?`,id);
        if(data){
            console.log('done',data);
            return data[0];
        } else{
            console.log('fail')
            return null;
        }
    }
    
}
