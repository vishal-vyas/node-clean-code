var expect  = require('chai').expect;
var request = require('request');
// const controller = require('../controller/controller');
// const movies = require('../Model/index');
describe('Status and content', function() {
    describe ('Movies Page - content type', function() {
        it('content-type', function(done){
            request('http://localhost:3000/movies', function(error, response, body) {
                expect(response.headers['content-type']).to.equal("application/json; charset=utf-8");
                done();
            });
        });
    });
});
