const express = require('express');
const app = express();
const controller = require('../src/controller');
// const finalpostMovie = require('../src/controller');
const makeExpressCallback = require('../src/exp-callback'); 
app.use(express.json());
app.get('/movie',makeExpressCallback(controller.finalMovie));
app.post('/movie',makeExpressCallback(controller.finalpostMovie));
app.put('/movie/:id',makeExpressCallback(controller.finalputMovie));

app.delete('/movie/:id',makeExpressCallback(controller.finaldelMovie));
app.listen(3000,()=>{
    console.log('server up and running on port 3000');
})