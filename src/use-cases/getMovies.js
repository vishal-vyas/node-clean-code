module.exports = async function makeGetMovies({moviesDB}){
    const response = await moviesDB.findAll()
    if(response){
     return {
        moviename : response.moviename,
        realeaseyear : response.realeaseyear
    };
}else{
    return false;
}
}