const moviesDB = require('../data-access');

const makeGetMovies = require('./getMovies');
const makePostMovie = require('./postMovie');
const makePutMovie  = require('./putMovies'); 
const makeDeleteMovie = require('./delMovies');
const submitMovie = makePostMovie({moviesDB});
// console.log(submitMovie)
const showMovie = makeGetMovies({moviesDB});
const putNewMovie = makePutMovie({moviesDB});

const delMovie = makeDeleteMovie({moviesDB})
// showMovie.then(res=>{
//     console.log(res);
// });
module.exports = {showMovie,submitMovie,putNewMovie,delMovie} 
// module.exports = submitMovie;
