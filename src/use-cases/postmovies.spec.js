
const cucumber = require('cucumber');

cucumber.When('I send a POST request to {string}, the data is', function (url, docString) {
    
    //console.log(docString);
    fetch(url,{headers: { 
        'Content-Type': 'application/json' },
        method:'POST',
        body: docString
    })
    .then(response =>response.text())
  });